resource "aws_instance" "app_server" {
  ami = "ami-0fa399d9c130ec923"
  subnet_id = module.vpc.public_subnets[0]
  instance_type = "t3.micro"

  associate_public_ip_address = true

  security_groups = ["${aws_security_group.public_app_server.id}"]

  tags = {
    Name = "Learning AWS 2023"
  }

  user_data = <<EOF
#!/bin/bash
echo "Copying the SSH Key to the server"
echo -e "${var.public_key}" >> /home/ec2-user/.ssh/authorized_keys
EOF  
}

resource "aws_security_group" "public_app_server" {
  name = "public-app-server"
  vpc_id = "${module.vpc.vpc_id}"
  ingress {
      cidr_blocks = [
        "0.0.0.0/0"
      ]
      from_port = 22
      to_port = 22
      protocol = "tcp"
    }
  // Terraform removes the default rule
    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

