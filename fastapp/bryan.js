'use strict'

import Fastify from 'fastify'
const fastify = Fastify({
  logger: true
})

export default async function (fastify, opts) {
    fastify.get('/', async (request, reply) => {
        return { hello: 'world' }
    })
}