# learn-aws-2023

I'm doing some practice of cloud computing concepts on AWS.

Ideas:
- Deploy a Node.js app on EC2
- Deploy that same app to a Docker container on EC2
- Deploy that container to Fargate
- Deploy that container to EKS

# Prerequisites

Before doing anything with AWS, I like the set up a couple of pre-requisites:
- An IAM User and Keypair for working with AWS APIs from my local machine
- A network to attach to my virtual machine and other resources

## Getting API and CLI access to my AWS account with an IAM User and Keypair

I created a new access key for my non-root user in the AWS Console for IAM. This gives me a pair labeled "Access key" and "Secret access key"
that I'm going to add to environment variables on my local machine for use with API calls.

https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html

I also chose to go ahead and install the AWS CLI, which will help me validate credentials (for one thing) and maybe do some other cool things
as I'm working with cloud resources. This is actually my first time using AWS CLI v2, so I might be a bit rusty as we go.

https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#cliv2-linux-install

I also like to keep this reference handy of AWS CLI commands - it is a great way to expore and self-discover the services themselves, too!

https://awscli.amazonaws.com/v2/documentation/api/latest/index.html

And the first call I always make to verify my AWS access via CLI is this one:

```bash
aws sts get-caller-identity
```

## Deciding how to automate management of infrastructure

The next bit of groundwork needed for exploring Web apps on AWS is to build a virtual private cloud (VPC) network
with a reasonable amount of security. I have a couple of ways to go here: I could use the AWS Console and its wizards to click my way
through creating the VPC and its resources, but what's the fun in that? I would rather use some automation so that I can tear down
and reconstruct my environment when I'm ready to play with it again.

But which automation technique should I use? Over the years, I've done quite a few:
- Write a bash script that makes AWS CLI calls
- Use AWS cloudformation
- Use Terraform
- Write an app which uses an AWS SDK in my preferred language to create resources

There are also some newer techniques that I haven't tried yet for managing resources, like AWS CDK. I think I'd like to brush up on
Terraform, though, because those skills are a bit more transferrable (i.e., beyond just AWS resources) and I expect to use more of that
on the job in the future.

That means I need to install a terraform client and figure out how to deal with my least favorite part of working with Terraform -
MANAGING STATE. (I guess you could say that's one of the hardest parts of computer science in general, though, right?) Installing is
simple enough:

https://developer.hashicorp.com/terraform/downloads

## Building out my VPC in Terraform

I spent some time reading up on the state of the art for automation network infrastructure management in AWS with Terraform. Given
that I want a flexible base for a few different types of development examples in this case (and that I don't want to spend all day
creating VPC resources) I thought I would try one of the AWS-provided Terraform VPC examples:

https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest/examples/simple

I was able to clone that example into this repo and adapt it a bit to get a sane VPC deployed with public and private subnets. I'm sure the
example is currently "overkill" for this repo, but I'd rather not worry about optimizing the network for now.

You can find the VPC configuration in [./terraform/main.tf], which makes heavy use of the publicly available "VPC" module provided by AWS.

# Part 1: Deploy a Node.js App on EC2

Now it's time to actually try our first project, which is to deploy an application in Node.js on an EC2 instance.


## Creating a Public EC2 Instance with Terraform

Before we get too far, we need an EC2 instance to play with. I added another terraform file [./terraform/instance.tf] which defines the basics
of any EC2 instance. Keep in mind that an "instance" here is just a virtual machine. Let's take a closer look at the Terraform configuration.

```json
resource "aws_instance" "app_server" {
  ami = "ami-0fa399d9c130ec923"
  subnet_id = module.vpc.public_subnets[0]
  instance_type = "t3.micro"

  associate_public_ip_address = true

  security_groups = ["${aws_security_group.public_app_server.id}"]

  user_data = <<EOF
#!/bin/bash
echo "Copying the SSH Key to the server"
echo -e "${var.public_key}" >> /home/ec2-user/.ssh/authorized_keys
EOF  

    tags = {
        Name = "Learning AWS 2023"
    }

}
```

Each entry here is really fundamental to understanding how EC2 instances (and thus, most compute resources on AWS) work.
Breaking it down further, I have to specify an Amazon Machine Image (AMI, or "ahh mee") which specifies the disk image to attach to the VM and a few other defaults. The
magic string you see here is something that I manually looked up from the "AMI Catalog" in the AWS Console for EC2. I chose to use an Amazon Linux 2023 AMI as my base image
for this exercise, knowing from experience that this image should have some sane defaults for working with services and other resources on Amazon's cloud.

Up next, I had to specify the Network Subnet in which to place the VM. For simplicity in the case of this part of the project, I used a public subnet. You (and your 
company's security officer) wouldn't want to use a public subnet in real life for any application tier, but if we push this to private we literally have extra "hops" to
jump through for every command and test from now on.

The instance type is also very important: Instance types define how much virtual hardware I get to associate with this VM, and thus, how much I'm having to pay AWS per
second for that hardware. The "micro" family is known for having "burstable compute" with price savings for testing and development workloads like what we are doing,
but AWS has many of these instance types available as choices: https://aws.amazon.com/ec2/instance-types/

Another non-default option we have to enable is to ask AWS to assign a public IPv4 IP address to the instance. This is necessary to let us get into the instance to
play around in our case; but in the industry, assigning public IP addresses is often avoided for security and cost implications. The IP which is assigned will be dynamic,
so that we need to check the AWS console (or the terraform outputs) to know how to log into our instance once available.

The EC2 concept of Security Groups is also critical to network security and reachability. You can see in [./terraform/instance.tf] that I have defined some rules related
to allowing inbound and outbound traffic on specific ports for this instance, as required for us to be able to log in over SSH, install OS-level packaging for development
and runtime of our sample application, and so on.

EC2 user data is another specification that we need just so that we can SSH into the instance. Technically, you can use this feature to run any number of commands upon
initial startup of your EC2 instance. The support is generic and included for instance configuration on boot. In practice, you would likely limit your use of EC2 user data
to the bare minimum and consider a complementary and more powerful automation tool such as Puppet or Chef for automating instance configuration. Once again, the default
tool is fine for our purpose. Within the Bash script that we're telling EC2 user data to run, I am adding my SSH public key to the default user of the Amazon Linux 2023
AMI, so that I can log into the instance with my SSH key. Even though it's my "public" key, I'm using a Terraform varaible to load my key from a local-only file of
variables (not checked in to Gitlab).

And finally, I have provided a Name for the instance using EC2 support for tags. This makes the instance easier to find in the AWS console. In practice, a more complete
set of tags could be used for tracking costs, applying configuration rules, and other purposes across a fleet of virtual machines.

## Creating a Sample App in Node.js

Now it's time to write a bit of application code. Node.js is probably my favorite language to play with right now, and I thought I would try to write a very simple Web 
application in that language for use as a sample app here.

I used play with Express for these types of projects , but I'm going to try to use Fastify for this sample app. From their 
(Github README)[https://github.com/fastify/fastify#quick-start], I was able to create a starter project and save it to [./fastify].

## Making an Installer for Linux

This may be pretty specific, but I would like to highlight the difference between running a production application on a VM as opposed to more modern or managed
tech stacks. My favorite way to do this has always been to create a proper Linux server and installer for production applications which embed any runtime components
required to limit challenges with versioning and dependency management. But I'll be honest that my skills in this hybrid of Ops and System Administration are a bit rusty!

Here is my plan of attack:
- Figure out how to get Fastify CLI to give me a standard Node.js entrypoint back from the project it created
- Turn that app into an executable
- Turn that executable into a Systemd service
- Create an RPM installer for that Systemd service









